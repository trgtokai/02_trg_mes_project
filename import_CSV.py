import pandas as pd
from sqlalchemy import create_engine

# 读取CSV文件
csv_file = 'D:\\BaiduSyncdisk\\Job\\mini-data-base\\09_init\\DataBase\\Export\\20240711_DataBase.csv'
df = pd.read_csv(csv_file)

# 连接到SQL数据库
server = 'your_server_name'
database = 'EX_DataBase'
username = 'your_username'
password = 'your_password'
driver = 'ODBC Driver 17 for SQL Server'

# 创建连接字符串
connection_string = f'mssql+pyodbc://{username}:{password}@{server}/{database}?driver={driver}'
engine = create_engine(connection_string)

# 将数据写入数据库表
table_name = 'dbo.Extrusion_db1'
df.to_sql(table_name, engine, if_exists='append', index=False)

print("数据导入完成")
