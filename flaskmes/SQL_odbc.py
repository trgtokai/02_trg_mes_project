import pyodbc

conn = None
def opensql(server = 'TRG14082-PC\SQLEXPRESS', database = 'MFG_DataBase', username = 'TRG-PE', password = '705705'):
    # 连接数据库参数
    # server = 'TRG-327-PC'  # 替换为你的SQL Server服务器名或IP地址  DESKTOP-QGKNIRA\SQLEXPRESS  172.18.136.183
    # server = 'TRG14082-PC\SQLEXPRESS'  #  172.18.137.155
    # database = 'PE_DataBase'      # 数据库名
    # username = 'TRG-PE'           # 登录名
    # password = '705705'          # 密码

    # 构建连接字符串
    conn_str = f'DRIVER={{ODBC Driver 17 for SQL Server}};SERVER={server};DATABASE={database};UID={username};PWD={password}'
    # 尝试连接数据库
    global conn

    try:
        conn = pyodbc.connect(conn_str)
        print("Successfully connected to the database.")
        sql_connection = True   # 输出SQL连接成功信号
        return conn

    except pyodbc.Error as e:
        print(f"Error connecting to database: {e}")
        sql_connection = False  # 输出SQL连接成功信号

def writesql(values1 = None, values2 = None, values3 = None,  values4 = None, values5 = None, values6 = None, values7 = None,):

    # 确认SQL连接状态
    # if sql_connection:
    global conn
    try:
        # 创建游标
        cursor = conn.cursor()
        print(cursor)

        # 执行插入示例
        cursor.execute("INSERT INTO AI_check_machine (时间, NG类型, 输出状态, 批次号, 作业员, 品番, 图片保存) VALUES (?, ?, ?, ?, ?, ?, ?)",
                       (f'{values1}', f'{values2}', f'{values3}', f'{values4}',
                        f'{values5}', f'{values6}', f'{values7}'))
        # print()
        conn.commit()
        print("Data successfully inserted.")

        # 关闭游标
        cursor.close()

    except pyodbc.Error as e:
        print(f"Error connecting to database: {e}")



def closesql():
    global conn

    # 关闭连接
    conn.close()
    print("Disconnected from the database.")


def readsql(read_line = 10):
    global conn
    try:
        # 创建游标
        cursor = conn.cursor()
        # 执行查询示例
        cursor.execute(f'SELECT TOP {read_line} * FROM [dbo].[ResinExtrusion1DataBase]')  # 替换为你的表名和查询语句
        # 获取查询结果
        rows = cursor.fetchall()
        for row in rows:
            print(row)
        # 关闭游标和连接
        cursor.close()
        return rows
    except pyodbc.Error as e:
        print(f"Error connecting to database: {e}")

def fetch_last_rows(server , database , username , password,x=10):
    # global conn
    conn = opensql(server , database , username , password )  # 调用数据库连接函数
    try:
        cursor = conn.cursor()  # 创建游标
        # query = "SELECT TOP 200 * FROM [dbo].[ResinExtrusion1DataBase] "  # 替换表名和列名ORDER BY [日期时间] DESC
        cursor.execute(f'SELECT TOP {x} * FROM [dbo].[ResinExtrusion1DataBase]') # 执行查询示例
        # print('execute query')
        rows = cursor.fetchall()
        cursor.close()  # 关闭数据库连接
        print("Fetched rows successfully")
        print(f"Data fetched: {rows[:5]}")  # 打印前5行数据进行调试
        return rows
    except pyodbc.Error as e:
        print(f"Error executing query: {e}")
        cursor.close()
        return None


if __name__ == '__main__':
    # opensql(server = 'TRG14082-PC\SQLEXPRESS', database = 'MFG_DataBase', username = 'TRG-PE', password = '705705')
    #
    # readsql(read_line = 10)
    fetch_last_rows(server = 'TRG14082-PC\SQLEXPRESS', database = 'MFG_DataBase',
                    username = 'TRG-PE', password = '705705',x=30)

# read_line = input("请输入读取行数")
# readsql(read_line)

