from flask import Flask, render_template_string, render_template,redirect, url_for
import pyodbc
# from SQL_odbc import *  ###数据库操作
import SQL_odbc  ###数据库操作
app = Flask(__name__,template_folder='templates')
# Flask 默认会在项目根目录下的 templates 文件夹中寻找模板。
# 如果你的模板文件位于 html 文件夹中，需要指定 template_folder
# process_time 网页的HTML模板
# process_time_template = "TRG-MES/html/process_time.html"
##### 数据库连接函数
# import pyodbc

@app.route('/')
def home():
    return render_template("home.html")

@app.route('/process_time')
def process_time():
    return render_template('process_time.html')

@app.route('/work_time')
def work_time():
    return render_template('process_time.html')
    # return render_template_string(work_time_template)

@app.route('/stop_reason')
def stop_reason():
    return "这里是異常停止理由页面"


@app.route('/production_management')
def production_management():
    return "这里是生産管理页面"


@app.route('/defect_aggregation')
def defect_aggregation():
    return "这里是不良集計页面"


@app.route('/reserve_feature')
def reserve_feature():
    return "这里是预留备用功能页面"


@app.route('/process_data')
def process_data():
    data = SQL_odbc.fetch_last_rows(server = 'TRG14082-PC\SQLEXPRESS', database = 'MFG_DataBase',
                                    username = 'TRG-PE', password = '705705',x=100)
    if data:
        return render_template('process_data.html', data=data)
    else:
        return "Failed to connect to the database or fetch data."


@app.route('/qa_data')
def qa_data():
    return "这里是QA数据页面"


@app.route('/iot_data')
def iot_data():
    return "这里是设备IOT页面"


if __name__ == '__main__':
    ### 公司有线网络  ↓
    app.run(host='172.18.137.155', port=5000, debug=True)
    # ### 公司wifi网络  ↓
    # app.run(host='172.18.138.110', port=5000, debug=True)
    ### 家里wifi网络  ↓
    # app.run(host='192.168.0.150', port=5000, debug=True)
    ### 家里有线网络  ↓
    # app.run(host='192.168.0.111', port=5000, debug=True)