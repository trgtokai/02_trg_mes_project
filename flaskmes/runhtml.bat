@echo off
setlocal

REM 设置当前目录为工作目录
cd /d "%~dp0"

REM 检查Python是否已安装
python --version >nul 2>&1
if %errorlevel% neq 0 (
    echo Python is not installed. Please install Python and try again.
    pause
    exit /b 1
)

REM 运行app.py
python app.py

endlocal
pause
